<?php
require 'security.php';
require 'database.php';

// pull out POST variables
$token = $_POST['token'];
$story_id = $_POST['story_id'];

// make sure the user has permissions to this resource
verify_user('stories', $story_id);
check_csrf($token);

// First delete all comments associated with the story
$stmt = $mysqli->prepare("DELETE FROM comments WHERE story_id = ?");
if(!$stmt){
    echo "Failed to create DELETE statement for story comments.";
}

// bind parameter to sql statement
$stmt->bind_param('i', $story_id);

// execute the statement and close the connection
$stmt->execute();
$stmt->close();

// Then delete the story itself
$stmt2 = $mysqli->prepare("DELETE FROM stories WHERE story_id = ?");
if(!$stmt2){
    echo "Failed to create DELETE statement for story.";
}

// bind parameter to sql statement
$stmt2->bind_param('i', $story_id);

// execute the statement and close it
$stmt2->execute();
$stmt2->close();

// Redirect to home page
header("Location: index2.php");
?>
