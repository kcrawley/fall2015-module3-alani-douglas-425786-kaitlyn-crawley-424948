<?php
require 'database.php';
require 'security.php';

// pull out POST variables
$token = $_POST['token'];
$comment_id = $_POST['comment_id'];

// make sure the user has permissions to this resource
verify_user('comments', $comment_id);
check_csrf($token);

// delete the comment
$stmt2 = $mysqli->prepare("DELETE FROM comments WHERE comment_id = ?");
if(!$stmt2)
    die("Failed to create DELETE statement for comment.");

// bind parameter to sql statement
$stmt2->bind_param('i', $comment_id);

// execute the statement and close the connection
$stmt2->execute();

// Redirect to home page
header("Location: index2.php");
?>
