<?php
require 'database.php';

if ( ! empty( $_POST ) ) {
    // Get username and password from form 
    $username = $_POST['newUser'];
    $password = crypt($_POST['newPassword']);

	$check_stmt = $mysqli->prepare("SELECT COUNT(username) FROM users WHERE username=?");
	if(!$check_stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$check_stmt->bind_param('s', $username);
	$check_stmt->execute();
	$check_stmt->bind_result($count);
	$check_stmt->fetch();
	$check_stmt->close();

	if($count > 0) {
		echo "Error - that username is already taken!";
		exit;
	}

    // Insert our data
	$stmt = $mysqli->prepare("insert into users values (?, ?)");
    if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
    $stmt->bind_param('ss', $username, $password);
 
    //Execute prepared statement
    $stmt->execute();
  
    // Close statement and connection
    $stmt->close();
}
session_start();
$_SESSION['user_id']=$username;
$_SESSION['token'] = substr(md5(rand()), 0, 10);
header("Location: index2.php");

?>
