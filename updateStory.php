<?php
require 'security.php';
require 'database.php';

session_start();

// Define values
$username=$_SESSION['user_id'];
$title=$_POST['title'];
$link=$_POST['link'];
$content=$_POST['story'];
$story_id=$_POST['story_id'];
$token=$_POST['token'];

// verify that the user has access to this resource
verify_user('stories', $story_id);
check_csrf($token);

// Insert our data
$stmt = $mysqli->prepare("UPDATE stories SET title=?, link=?, content=? WHERE story_id=?");
if(!$stmt){
    echo "Error in preparing story update statement";
	exit;
}
$stmt->bind_param('sssi', $title, $link, $content, $story_id);
     
// execute and close statement
$stmt->execute();
$stmt->close();

// Redirect to home page
header("Location: index2.php");
?>
