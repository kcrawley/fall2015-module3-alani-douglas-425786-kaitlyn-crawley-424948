<?php
require 'security.php';
require 'database.php';

session_start();

// pull out POST variables
$username=$_SESSION['user_id'];
$title=$_POST['title'];
$link=$_POST['link'];
$content=$_POST['story'];
$token=$_POST['token'];

// check for CSRF attacks
check_csrf($token);

// Insert our data
$stmt = $mysqli->prepare("INSERT INTO stories (title, author, link, content) values (?, ?, ?, ?)");
if(!$stmt)
    die("Error in preparing statement for story insert.");

$stmt->bind_param('ssss', $title, $username, $link, $content);
     
// execute and close statement
$stmt->execute();
$stmt->close();

// Redirect to home page
header("Location: index2.php");
?>
