<!DOCTYPE html>
<html>     
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" rel="stylesheet" href="index.css">
        <title>News Site</title>
    </head>
    
    <body>
       <div id="header">
            <ul id="navBar">
                <li id="logo">NEWS SITE</li>
                <li><a href="index2.php" id="navBar">Home</a></li>
                <li><a href="favorites.php" id="navBar">Favorites</a></li>
                <li><a href="profilePage.php" id="navBar">My Profile</a></li>
            </ul>
        </div>

        <?php
			session_start();
			$token = $_SESSION['token'];
			
            require 'database.php';

            if(!isset($_SESSION['user_id'])){
                //<!-- user log in -->
                echo '<div id=login>';
                echo '<h3>Log in for existing users:</h3>';
                echo '<form action="login.php" method="POST">';
                echo '<label>Username: <input type="text" name="username"></label>';
                echo '<label>Password: <input type="password" name="password"></label>';
                echo '<input type="submit" value="Submit"/>';
                echo '</form>';
		
                //<!--Create a new user -->
                echo '<h3>Create a new user:</h3>';
                echo '<form action="createUser.php" method="POST">';
                echo '<label>Username:<input type="text" name="newUser"/></label>';
                echo '<label>Password:<input type="password" name="newPassword"/></label>';
                echo '<input type="submit" value="Create User"/>';
                echo '</form>';
            }
            
            else{
                echo '<h6 id=login></h6>';
                echo '<form action="logout.php" method="GET">';
				echo '<input type="submit" value="Log Out"/>';
                echo '</form>';
                echo '</div>';
            }
            
            function printComments($story_id, $mysqli) {
            	$stmt = $mysqli->prepare("select * from comments where story_id=? order by comment_id");
            	if(!$stmt){
            	        printf("Query prep Failed: %s\n", $mysqli->error);
            	}
				$stmt->bind_param("i", $story_id);
            	$stmt->execute();
            	$stmt->bind_result($author, $story_id, $comment_id, $comment);

            	echo "<ul>";
            	while($stmt ->fetch()){
            	    echo "<li>";
					echo "<div class=\"comment\">";
					printf("%s says:<br>",
						htmlspecialchars($author));
					echo htmlspecialchars($comment);
					echo "<br/><br/>";

                	if(isset($_SESSION['user_id']) && $_SESSION['user_id']==$author){
					 	editComment($comment_id);
                     	deleteComment($comment_id);
                	}
            	    echo "</div>";
            	    echo "</li><br>";
            	}
            	echo "</ul>";
            	$stmt->close();
			}
            
          function deleteStory($story_id){
		  		global $token;
				echo "<div class=\"storybutton\">";
				echo '<form action="deleteStory.php" method="POST">';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Delete"/>';
                echo '</form>';
				echo '</div>';
			}

			function editStory($story_id) {
		  		global $token;
				echo "<div class=\"storybutton\">";
                echo '<form action="editStory.php" method="GET">';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Edit"/>';
                echo '</form>';
				echo '</div>';
			}
			
			function favorite($title, $author, $link, $story_id, $content){
				global $token;
				echo "<div class=\"storybutton\">";
                echo '<form action="addFavorite.php" method="POST">';
				echo '<input type="hidden" name="title" value="'.$title.'"/>';
				echo '<input type="hidden" name="author" value="'.$author.'"/>';
				echo '<input type="hidden" name="link" value="'.$link.'"/>';
				echo '<input type="hidden" name="content" value="'.$content.'"/>';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Favorite"/>';
                echo '</form>';
				echo '</div>';
			}
            
          function comment($story_id){
		  		global $token;
		  		echo '<div class="commentForm">';
                echo '<form action="submitComment.php" method="POST">';
                echo '<input type="text" name="comment" style="width: 250px; height: 50px;"/>';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Comment" class="commentButton"/>';
                echo '</form>';
				echo '</div>';
            }

          	function editComment($comment_id) { 
		  		global $token;
				echo '<div class="buttonForm">';
				echo '<form action="editComment.php" method="GET">';
                echo '<input type="hidden" name="comment_id" value="'.$comment_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
				echo '<input type="submit" value="Edit" class="editCommentButton"/>';
                echo '</form>';
				echo '</div>';
			}

			function deleteComment($comment_id) {
		  		global $token;
				echo '<div class="buttonForm">';
				echo '<form action="deleteComment.php" method="POST">';
                echo '<input type="hidden" name="comment_id" value="'.$comment_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Delete" class="editCommentButton"/>';
                echo '</form>';
				echo '</div>';
			}

//-------------------------------------------------------------------------------

            $stmt = $mysqli->prepare("select * from stories");
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->execute();
            
            $stmt->bind_result($title, $author, $story_id, $link, $content);
            $stmt->store_result();

            echo "<ul>";
            while($stmt ->fetch()){
                echo "<li>";
				echo "<div class=\"story\">";
				printf("<span class=\"storyTitle\">%s</span><br>", htmlspecialchars($title));
				printf("<span class=\"storyAuthor\">%s <br>",
					htmlspecialchars($author));
				printf("<a href=\"%s\">%s</a><br><br>",
					htmlspecialchars($link), htmlspecialchars($link));
				printf("<span class=\"storyContent\">%s</span><br>",
					htmlspecialchars($content));

				echo "<br/>";

                if(isset($_SESSION['user_id']) && $_SESSION['user_id']==$author){
					editStory($story_id);
                    deleteStory($story_id);
                }

                if(isset($_SESSION['user_id']))
					favorite($title, $author, $link, $story_id, $content);

                echo "</div>";
                echo "</li>";
                
                printComments($story_id, $mysqli);
 
                if(isset($_SESSION['user_id'])){
                    comment($title, $author, $link, $story_id);
                }
            }

            echo "</ul>";
            $stmt->close();
        ?>
    </body>
</html>
