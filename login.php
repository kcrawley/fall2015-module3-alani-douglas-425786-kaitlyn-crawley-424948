<?php
session_start();
require 'database.php';
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
 
// Bind the parameter
$user = $_POST['username'];
$stmt->bind_param('s', $user);

$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $username, $pwd_hash);
$stmt->fetch();

$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
//if( $user==$username && $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
if(crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	session_start();

	// Login succeeded!
	$_SESSION['user_id'] = $user;
	$_SESSION['token'] = substr(md5(rand()), 0, 10);

	// Redirect to your target page
	header("Location: index2.php");
}
else{
	// Login failed; redirect back to the login screen
	echo str_repeat("<br/>", 8) . "Invalid Username or Password.";
	exit;
}
?>
