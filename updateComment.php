<?php
require 'security.php';
require 'database.php';

session_start();

// pull out POST variables
$username=$_SESSION['user_id'];
$comment=$_POST['comment'];
$comment_id=$_POST['comment_id'];
$token=$_POST['token'];

// verify that the user has access to this resource
verify_user('comments', $comment_id);
check_csrf($token);

// Insert our data
$stmt = $mysqli->prepare("UPDATE comments SET comment=? WHERE comment_id=?");
if(!$stmt){
    echo "Error in preparing comment update statement";
	exit;
}
$stmt->bind_param('si', $comment, $comment_id);
     
// execute and close statement
$stmt->execute();
$stmt->close();

// Redirect to home page
header("Location: index2.php");
?>
