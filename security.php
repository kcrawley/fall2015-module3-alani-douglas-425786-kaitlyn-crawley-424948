<?php

require 'database.php';

/*
	function to compare the token of a request with the token saved in
	the session to detect and stop CSRF
*/
function check_csrf($post_token) {
	global $mysqli;
	if(session_id() == '' || !isset($_SESSION))
		session_start();

	if(!isset($_SESSION['token']) || ($post_token !== $_SESSION['token'])) {
		die('CSRF token does not match, discarding request!');
	}
}

/*
	function to verify that the session user has admin privs for the
	specified resource in the SQL database
*/
function verify_user($table, $id) {
	global $mysqli;

	if(session_id() == '' || !isset($_SESSION))
		session_start();

	if(!isset($_SESSION['user_id'])) {
		echo "You do not have permission to resource with id: ";
		echo $id;
		exit;
	}

	$cmd = "";
	if($table === "stories") {
		$cmd = "SELECT author FROM stories WHERE story_id=?";
	} else if($table === "comments") {
		$cmd = "SELECT author FROM comments WHERE comment_id=?";
	} else if($table ==="favorites") {
		$cmd = "SELECT username FROM favorites WHERE favorite_id=?";
	}

	$s = $mysqli->prepare($cmd);
	if(!$s) {
		echo "Error in preparing verify_user statement";
	}

	$s->bind_param('i', $id);
	$s->execute();
	$s->bind_result($author);
	$s->fetch();

	if($author !== $_SESSION['user_id']) {
		echo "You do not have permission to resource with id: ";
		echo $id;
		exit;
	}

	$s->close();
}
?>
