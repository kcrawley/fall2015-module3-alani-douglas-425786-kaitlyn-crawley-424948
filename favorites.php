<!DOCTYPE html>
<html>
    <head>
        <title>User Favorites Page</title>
        <link type="text/css" rel="stylesheet" href="index.css">
        <meta charset="UTF-8"/>
    </head>
    <body>
        <div id="header">
            <ul id="navBar">
                <li id="logo">NEWS SITE</li>
                <li><a href="index2.php" id="navBar">Home</a></li>
                <li><a href="favorites.php" id="navBar">Favorites</a></li>
                <li><a href="profilePage.php" id="navBar">My Profile</a></li>
            </ul>
        </div>
        <?php
            require 'database.php';

			session_start();

			$token = $_SESSION['token'];
			
			if(!isset($_SESSION['user_id'])){
				echo '<h1 id="login">You must be logged in to use this feature. Log in or create an account on the home page</h1>';
				exit;
			}
            
            function deleteFavorite($favorite_id){
                global $token;
                echo "<div class=\"storybutton\">";
                echo '<form action="deleteFavorite.php" method="POST">';
                echo '<input type="hidden" name="favorite_id" value="'.$favorite_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
 				echo '<input type="submit" value="Unfavorite"/>';
                echo '</form>';
                echo '</div>';
            }

          	function deleteStory($story_id){
                global $token;
                echo "<div class=\"storybutton\">";
                echo '<form action="deleteStory.php" method="POST">';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Delete"/>';
                echo '</form>';
                echo '</div>';
            }

            function editStory($story_id) {
                global $token;
                echo "<div class=\"storybutton\">";
                echo '<form action="editStory.php" method="GET">';
                echo '<input type="hidden" name="story_id" value="'.$story_id.'"/>';
                echo '<input type="hidden" name="token" value="'.$token.'"/>';
                echo '<input type="submit" value="Edit"/>';
                echo '</form>';
                echo '</div>';
            }
		
            $stmt = $mysqli->prepare("SELECT title, author, stories.story_id, link, content, favorite_id FROM favorites JOIN stories ON favorites.story_id = stories.story_id WHERE favorites.username=?");
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            
			$stmt->bind_param('s', $_SESSION['user_id']);
            $stmt->execute();
            $stmt->bind_result($title, $author, $story_id, $link, $content, $favorite_id);
            $stmt->store_result();

            echo "<ul>";
            while($stmt ->fetch()){
                echo "<li>";
                echo "<div class=\"story\">";
                printf("<span class=\"storyTitle\">%s</span><br>", htmlspecialchars($title));
                printf("<span class=\"storyAuthor\">%s <br>",
                    htmlspecialchars($author));
                printf("<a href=\"%s\">%s</a><br><br>",
                    htmlspecialchars($link), htmlspecialchars($link));
                printf("<span class=\"storyContent\">%s</span><br>",
                    htmlspecialchars($content));
                if(isset($_SESSION['user_id'])){
                    deleteFavorite($favorite_id);
                }
                
                if($_SESSION['user_id']==$author){
                    editStory($story_id);
                    deleteStory($story_id);
                }
                echo "</div>";
                echo "</li>";
            }
            echo "</ul>";
            $stmt->close();
        ?>
        
    </body>
</html>
