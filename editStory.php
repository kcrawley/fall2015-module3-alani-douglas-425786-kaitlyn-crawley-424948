<!DOCTYPE html>
<html>
    <head>
        <title>User Profile Page</title>
        <link type="text/css" rel="stylesheet" href="index.css">
        <meta charset="UTF-8"/>
    </head>
    <body>
        <div id="header">
            <ul id="navBar">
                <li id="logo">NEWS SITE</li>
                <li><a href="index2.php" id="navBar">Home</a></li>
                <li><a href="favorites.html" id="navBar">Favorites</a></li>
                <li><a href="profilePage.php" id="navBar">My Profile</a></li>
            </ul>
        </div>

        <?php
			require 'database.php';

			session_start();

			$stmt = $mysqli->prepare("SELECT * FROM stories WHERE story_id=? LIMIT 1");
			if(!$stmt) {
				echo "error in preparing statement";
			}

			$stmt->bind_param('i', $_GET['story_id']);
			$stmt->execute();
			$stmt->bind_result($title, $author, $story_id, $link, $content);
			$stmt->fetch();
        ?>
        
        <!-- Submit a story here -->
        <h3 id="submitStory">Submit a Story:</h3>
        <form action="updateStory.php" method="POST">
            <label>Title: <input type="text" name="title" value=<?php echo '"'.htmlspecialchars($title).'"'?>>
			</input></label><br>
            <label>Link: <input type="text" name="link" value=<?php echo '"'.htmlspecialchars($link).'"'?>>
			</input></label><br>
            <label>Story: <input type="text" name="story" maxlength="70" style="width: 300px; height: 100px;" value=<?php echo '"'.htmlspecialchars($content).'"'?>>
			</input></label>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>" />
            <input type="hidden" name="story_id" value="<?php echo $story_id ?>" />
            <input type="submit" value="Submit"/>
        </form>

		<?php $stmt->close() ?>
    </body>
</html>
