<!DOCTYPE html>
<html>
    <head>
        <title>User Profile Page</title>
        <link type="text/css" rel="stylesheet" href="index.css">
        <meta charset="UTF-8"/>
    </head>
    <body>
        <div id="header">
            <ul id="navBar">
                <li id="logo">NEWS SITE</li>
                <li><a href="index2.php" id="navBar">Home</a></li>
                <li><a href="favorites.html" id="navBar">Favorites</a></li>
                <li><a href="profilePage.php" id="navBar">My Profile</a></li>
            </ul>
        </div>

        <?php
			require 'database.php';

			session_start();

			$stmt = $mysqli->prepare("SELECT * FROM comments WHERE comment_id=? LIMIT 1");
			if(!$stmt) {
				echo "error in preparing statement";
			}

			$stmt->bind_param('i', $_GET['comment_id']);
			$stmt->execute();
			$stmt->bind_result($author, $story_id, $comment_id, $comment);
			$stmt->fetch();
        ?>
        
        <!-- Submit a story here -->
        <form action="updateComment.php" method="POST">
            <label>Edit comment: <input type="text" name="comment" style="width: 300px; height: 100px;" value=<?php echo '"'.htmlspecialchars($comment).'"'?>></input></label>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'] ?>" />
            <input type="hidden" name="comment_id" value="<?php echo $comment_id ?>" />
            <input type="submit" value="Submit"/>
        </form>

		<?php $stmt->close() ?>
    </body>
</html>
