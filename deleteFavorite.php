<?php

require 'security.php';
require 'database.php';

// pull out POST variables
$token = $_POST['token'];
//$story_id = $_POST['story_id'];
$favorite_id = $_POST['favorite_id'];

// make sure the user has permissions to this resource
verify_user('favorites', $favorite_id);
check_csrf($token);

// Then delete the favorite
$stmt2 = $mysqli->prepare("DELETE FROM favorites WHERE favorite_id=?");
if(!$stmt2){
    echo "Failed to create DELETE statement for story.";
}

// bind parameter to sql statement
$stmt2->bind_param('i', $favorite_id);

// execute the statement and close it
$stmt2->execute();
$stmt2->close();

// Redirect to home page
header("Location: favorites.php");
?>
