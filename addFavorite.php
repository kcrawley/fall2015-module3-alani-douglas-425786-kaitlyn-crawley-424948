<?php
require 'security.php';
require 'database.php';

session_start();

// pull out POST variables
$username=$_SESSION['user_id'];
$story_id=$_POST['story_id'];
$token=$_POST['token'];

// check for CSRF attacks
check_csrf($token);

$check_stmt = $mysqli->prepare("SELECT COUNT(username) FROM favorites WHERE username=? AND story_id=?");
if(!$check_stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}

$check_stmt->bind_param('si', $username, $story_id);
$check_stmt->execute();
$check_stmt->bind_result($count);
$check_stmt->fetch();
$check_stmt->close();

if($count > 0) {
	header("Location: favorites.php");
	exit;
}

// Insert our data
$stmt = $mysqli->prepare("INSERT INTO favorites (username, story_id) VALUES (?, ?)");
if(!$stmt){
    die("Unable to add to favorites");
}

$stmt->bind_param('si', $username, $story_id);

// execute statement and then close
$stmt->execute();
$stmt->close();

//Redirect to home page
header("Location: favorites.php");
?>
