<?php
require 'security.php';
require 'database.php';

session_start();

// pull out POST variables
$username=$_SESSION['user_id'];
$story_id=$_POST['story_id'];
$comment=$_POST['comment'];
$token=$_POST['token'];

// check for CSRF attacks
check_csrf($token);

// Insert our data
$stmt = $mysqli->prepare("INSERT INTO comments (author, story_id, comment) values (?, ?, ?)");
if(!$stmt){
    echo "Unable to upload comment";
}

$stmt->bind_param('sis', $username, $story_id, $comment);
     
// execute statement and then close
$stmt->execute();
$stmt->close();

// Redirect to home page
header("Location: index2.php");
?>
